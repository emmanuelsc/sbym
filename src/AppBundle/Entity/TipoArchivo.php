<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 14/04/18
 * Time: 06:13 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tipo_archivo")
 */
class TipoArchivo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nombre;

    /**
     * @ORM\Column(type="text")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $abreviatura;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Archivo", mappedBy="tipoArchivo")
     */
    private $archivos;

    public function __construct()
    {
        $this->archivos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion($descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    public function addArchivo(\AppBundle\Entity\Archivo $archivo): ?TipoArchivo
    {
        $this->archivos[] = $archivo;

        return $this;
    }

    public function removeArchivo(\AppBundle\Entity\Archivo $archivo): bool
    {
        return $this->archivos->removeElement($archivo);
    }

    public function getArchivos(): ?Collection
    {
        return $this->archivos;
    }

    public function getAbreviatura(): ?string
    {
        return $this->abreviatura;
    }

    public function setAbreviatura($abreviatura): void
    {
        $this->abreviatura = $abreviatura;
    }
}
