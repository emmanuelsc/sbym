<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 1/04/18
 * Time: 08:12 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="contacto")
 */
class Contacto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $extension;

    /**
     * @ORM\Column(type="string", length=17)
     */
    private $celular;

    /**
     * @ORM\Column(type="string", length=35)
     */
    private $correo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dependencia", inversedBy="contactos")
     * @ORM\JoinColumn(name="id_dependencia", referencedColumnName="id")
     */
    private $dependencia;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setNombre($nombre): Contacto
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setTelefono($telefono): Contacto
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setExtension($extension): Contacto
    {
        $this->extension = $extension;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setCelular($celular): Contacto
    {
        $this->celular = $celular;

        return $this;
    }

    public function getCelular(): ?string
    {
        return $this->celular;
    }

    public function setCorreo($correo): Contacto
    {
        $this->correo = $correo;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setDependencia(\AppBundle\Entity\Dependencia $dependencia = null): Contacto
    {
        $this->dependencia = $dependencia;

        return $this;
    }

    public function getDependencia(): ?Dependencia
    {
        return $this->dependencia;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones($observaciones): Contacto
    {
        $this->observaciones = $observaciones;

        return $this;
    }
}
