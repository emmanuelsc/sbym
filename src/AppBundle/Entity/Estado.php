<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 25/03/18
 * Time: 10:20 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="estado")
 */
class Estado
{
    const UP = 1;
    const DOWN = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=22, nullable=false)
     */
    private $nombre;

    public function getId(): int
    {
        return $this->id;
    }

    public function setNombre($nombre): Estado
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre(): string
    {
        return $this->nombre;
    }
}
