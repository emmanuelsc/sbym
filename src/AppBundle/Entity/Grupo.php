<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 27/03/18
 * Time: 06:10 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="grupo")
 */
class Grupo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dependencia", inversedBy="grupos")
     * @ORM\JoinColumn(name="id_dependencia", referencedColumnName="id", nullable=true)
     */
    private $dependencia;

    public function getId(): int
    {
        return $this->id;
    }

    public function setNombre($nombre): Grupo
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setDependencia(\AppBundle\Entity\Dependencia $dependencia = null): Grupo
    {
        $this->dependencia = $dependencia;

        return $this;
    }

    public function getDependencia(): ?Dependencia
    {
        return $this->dependencia;
    }
}
