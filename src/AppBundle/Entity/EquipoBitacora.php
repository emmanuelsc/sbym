<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 21/04/18
 * Time: 05:52 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="equipo_bitacora")
 */
class EquipoBitacora
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $equipo;

    /**
     * @ORM\Column(type="string")
     */
    private $usuario;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $campo;

    /**
     * @ORM\Column(type="string")
     */
    private $old;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $new;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $tabla;

    public function getId(): int
    {
        return $this->id;
    }

    public function setEquipo($equipo): EquipoBitacora
    {
        $this->equipo = $equipo;

        return $this;
    }

    public function getEquipo(): string
    {
        return $this->equipo;
    }

    public function setUsuario($usuario): EquipoBitacora
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getUsuario(): string
    {
        return $this->usuario;
    }

    public function setFecha($fecha): EquipoBitacora
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getFecha(): \DateTime
    {
        return $this->fecha;
    }

    public function getCampo(): string
    {
        return $this->campo;
    }

    public function setCampo($campo): void
    {
        $this->campo = $campo;
    }

    public function getOld(): string
    {
        return $this->old;
    }

    public function setOld($old): void
    {
        $this->old = $old;
    }

    public function getNew(): string
    {
        return $this->new;
    }

    public function setNew($new): void
    {
        $this->new = $new;
    }

    public function getTabla(): string
    {
        return $this->tabla;
    }

    public function setTabla($tabla): void
    {
        $this->tabla = $tabla;
    }
}
