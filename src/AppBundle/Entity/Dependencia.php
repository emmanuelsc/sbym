<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 27/03/18
 * Time: 05:50 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;


use AppBundle\IBitacora;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DependenciaRepository")
 * @ORM\Table(name="dependencia")
 */
class Dependencia implements IBitacora
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $campus;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $nodo;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $vlans;

    /**
     * @ORM\Column(type="string", length=17, nullable=true)
     */
    private $red;

    /**
     * @ORM\Column(type="string", length=17, nullable=true)
     */
    private $dhcp;

    /**
     * @ORM\Column(type="string", length=17, nullable=true)
     */
    private $mdhcp;

    /**
     * @ORM\Column(type="string", length=17, nullable=true)
     */
    private $gwdhcp;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaActualizacion;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Grupo", mappedBy="dependencia")
     */
    private $grupos;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contacto", mappedBy="dependencia")
     */
    private $contactos;

    public function getId(): int
    {
        return $this->id;
    }

    public function setNombre($nombre): Dependencia
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setCampus($campus): Dependencia
    {
        $this->campus = $campus;

        return $this;
    }

    public function getCampus(): ?string
    {
        return $this->campus;
    }

    public function setNodo($nodo): Dependencia
    {
        $this->nodo = $nodo;

        return $this;
    }

    public function getNodo(): ?string
    {
        return $this->nodo;
    }

    public function setVlans($vlans = null): Dependencia
    {
        $this->vlans = $vlans;

        return $this;
    }

    public function getVlans(): ?string
    {
        return $this->vlans;
    }

    public function setRed($red = null): Dependencia
    {
        $this->red = $red;

        return $this;
    }

    public function getRed(): ?string
    {
        return $this->red;
    }

    public function setDhcp($dhcp = null): Dependencia
    {
        $this->dhcp = $dhcp;

        return $this;
    }

    public function getDhcp(): ?string
    {
        return $this->dhcp;
    }

    public function setMdhcp($mdhcp = null): Dependencia
    {
        $this->mdhcp = $mdhcp;

        return $this;
    }

    public function getMdhcp(): ?string
    {
        return $this->mdhcp;
    }

    public function setGwdhcp($gwdhcp = null): Dependencia
    {
        $this->gwdhcp = $gwdhcp;

        return $this;
    }

    public function getGwdhcp(): ?string
    {
        return $this->gwdhcp;
    }

    public function setFechaActualizacion($fechaActualizacion): Dependencia
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTime
    {
        return $this->fechaActualizacion;
    }

    public function setObservaciones($observaciones = null): Dependencia
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function __construct()
    {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contactos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addGrupo(\AppBundle\Entity\Grupo $grupo): ?Dependencia
    {
        if($this->grupos->contains($grupo)) {
            return null;
        }

        $this->grupos[] = $grupo;
        $grupo->setDependencia($this);

        return $this;
    }

    public function removeGrupo(\AppBundle\Entity\Grupo $grupo): bool
    {
        $result = $this->grupos->removeElement($grupo);
        $grupo->setDependencia(null);

        return $result;
    }

    public function getGrupos(): ?Collection
    {
        return $this->grupos;
    }

    public function addContacto(\AppBundle\Entity\Contacto $contacto): Dependencia
    {
        $this->contactos[] = $contacto;

        return $this;
    }

    public function removeContacto(\AppBundle\Entity\Contacto $contacto): bool
    {
        return $this->contactos->removeElement($contacto);
    }

    /**
     * Get contactos.
     *
     * @return ArrayCollection|Contacto[]
     */
    public function getContactos(): Collection
    {
        return $this->contactos;
    }

    public function getTabla()
    {
        return 'Dependencia';
    }
}
