<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 25/03/18
 * Time: 10:10 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;

use AppBundle\IBitacora;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EquipoRepository")
 * @ORM\Table(name="equipo")
 */
class Equipo implements IBitacora
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="id_usuario", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $usuario;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    private $mascara;

    /**
     * @ORM\Column(type="string", length=15, nullable=false)
     */
    private $gateway;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estado")
     * @ORM\JoinColumn(name="id_estado", referencedColumnName="id", nullable=false)
     */
    private $estado;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ubicacion;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $inventario;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=17, nullable=false)
     */
    private $mac;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Marca")
     * @ORM\JoinColumn(name="id_marca", referencedColumnName="id", nullable=false)
     */
    private $marca;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Modelo")
     * @ORM\JoinColumn(name="id_modelo", referencedColumnName="id", nullable=false)
     */
    private $modelo;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $fechaActualizacion;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoEquipo")
     * @ORM\JoinColumn(name="id_tipo_equipo", referencedColumnName="id", nullable=false)
     */
    private $tipoEquipo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Grupo", inversedBy="equipo")
     * @ORM\JoinColumn(name="id_grupo", referencedColumnName="id", nullable=true)
     */
    private $grupo;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Equipo")
     * @ORM\JoinColumn(name="id_controladora", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $controladora;


    public function getId(): int
    {
        return $this->id;
    }

    public function setNombre($nombre): Equipo
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setIp($ip): Equipo
    {
        $this->ip = $ip;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setMascara($mascara): Equipo
    {
        $this->mascara = $mascara;

        return $this;
    }

    public function getMascara(): ?string
    {
        return $this->mascara;
    }

    public function setGateway($gateway): Equipo
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getGateway(): ?string
    {
        return $this->gateway;
    }

    public function setUbicacion($ubicacion): Equipo
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    public function getUbicacion(): ?string
    {
        return $this->ubicacion;
    }

    public function setInventario($inventario): Equipo
    {
        $this->inventario = $inventario;

        return $this;
    }

    public function getInventario(): ?string
    {
        return $this->inventario;
    }

    public function setSerie($serie): Equipo
    {
        $this->serie = $serie;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setMac($mac): Equipo
    {
        $this->mac = $mac;

        return $this;
    }

    public function getMac(): ?string
    {
        return $this->mac;
    }

    public function setFechaActualizacion($fechaActualizacion): Equipo
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTime
    {
        return $this->fechaActualizacion;
    }

    public function setObservaciones($observaciones): Equipo
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setUsuario(\AppBundle\Entity\Usuario $usuario): Equipo
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setEstado(\AppBundle\Entity\Estado $estado): Equipo
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstado(): ?Estado
    {
        return $this->estado;
    }

    public function setMarca(\AppBundle\Entity\Marca $marca): Equipo
    {
        $this->marca = $marca;

        return $this;
    }

    public function getMarca(): ?Marca
    {
        return $this->marca;
    }

    public function setModelo(\AppBundle\Entity\Modelo $modelo): Equipo
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getModelo(): ?Modelo
    {
        return $this->modelo;
    }

    public function setTipoEquipo(\AppBundle\Entity\TipoEquipo $tipoEquipo): Equipo
    {
        $this->tipoEquipo = $tipoEquipo;

        return $this;
    }

    public function getTipoEquipo(): ?TipoEquipo
    {
        return $this->tipoEquipo;
    }

    public function setGrupo(\AppBundle\Entity\Grupo $grupo = null): Equipo
    {
        $this->grupo = $grupo;

        return $this;
    }

    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    public function setControladora(\AppBundle\Entity\Equipo $controladora = null): Equipo
    {
        $this->controladora = $controladora;

        return $this;
    }

    public function getControladora(): ?Equipo
    {
        return $this->controladora;
    }

    public function getTabla()
    {
        return $this->getTipoEquipo()->getNombre();
    }
}
