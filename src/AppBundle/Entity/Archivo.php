<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 14/04/18
 * Time: 06:14 PM
 */
declare(strict_types=1);
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="archivo")
 */
class Archivo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="id_usuario", referencedColumnName="id", onDelete="SET NULL")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoArchivo", inversedBy="archivos")
     * @ORM\JoinColumn(name="id_tipo_archivo", referencedColumnName="id", nullable=false)
     */
    private $tipoArchivo;

    private $file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl($url): void
    {
        $this->url = $url;
    }

    public function setUsuario(\AppBundle\Entity\Usuario $usuario = null): ?Archivo
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setTipoArchivo(\AppBundle\Entity\TipoArchivo $tipoArchivo = null): Archivo
    {
        $this->tipoArchivo = $tipoArchivo;

        return $this;
    }

    public function getTipoArchivo(): ?TipoArchivo
    {
        return $this->tipoArchivo;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file): void
    {
        $this->file = $file;
    }
}
