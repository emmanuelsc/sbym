<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 14/04/18
 * Time: 07:01 PM
 */
declare(strict_types=1);
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, $tipoArchivo)
    {
        $fileName = $tipoArchivo . '_' . md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTargetDirectory(), $fileName);

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}