<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 27/03/18
 * Time: 07:03 PM
 */

namespace AppBundle;


final class ArregloDependencia
{
    const CAMPUS = [
        'CCH',
        'CU',
        'ENP',
        'EXT',
        'FES',
        'FOR'
    ];

    const NODOS = [
        'CCH',
        'ARQ',
        'DGTIC',
        'IIMAS',
        'ZC',
        'ENP',
        'EXT',
        'FES',
        'FOR'
    ];
}