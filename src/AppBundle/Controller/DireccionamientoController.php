<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 7/04/18
 * Time: 04:16 PM
 */
declare(strict_types=1);
namespace AppBundle\Controller;


use AppBundle\Entity\Dependencia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DireccionamientoController extends Controller
{
    /**
     * @Route("/direccionamiento/{campus}/{nodo}", name="direccionamiento")
     */
    public function listar(string $campus, string $nodo)
    {

        $dependencias = $this->getDoctrine()
            ->getRepository(Dependencia::class)
            ->getDependenciasPorCampusNodo($campus, $nodo);

        return $this->render('Direccionamiento/listar.html.twig', [
            'nodo' => $nodo,
            'campus' => $campus,
            'dependencias' => $dependencias
        ]);
    }

    /**
     * @Route("/direccionamiento/dependencia/{dependencia}/detalle", name="direccionamiento.dependecia_detalle")
     */
    public function dependenciaDetalle(Dependencia $dependencia)
    {
        $aps = $this->getDoctrine()->getRepository(Dependencia::class)
            ->getAppsPorDependencia($dependencia->getId());

        return $this->render('Direccionamiento/dependencia_detalle.html.twig', [
            'dependencia' => $dependencia,
            'aps' => $aps
        ]);
    }
}