<?php

declare(strict_types=1);

namespace AppBundle\Controller;

use AppBundle\ArregloDependencia;
use AppBundle\Entity\Dependencia;
use AppBundle\Entity\Equipo;
use AppBundle\Entity\Estado;
use AppBundle\Entity\TipoEquipo;
use AppBundle\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="default.index")
     */
    public function index()
    {
        if($this->isGranted('ROLE_INVITADO')) {

            $aps = $this->getDoctrine()
                ->getRepository(Equipo::class)
                ->findBy([
                    'tipoEquipo' => TipoEquipo::AP
                ]);

            $servidores = $this->getDoctrine()
                ->getRepository(Equipo::class)
                ->findBy([
                    'tipoEquipo' => TipoEquipo::SERVIDOR
                ]);

            $controladoras = $this->getDoctrine()
                ->getRepository(Equipo::class)
                ->findBy([
                    'tipoEquipo' => TipoEquipo::CONTROLADOR
                ]);

            return $this->render('Default/index_invitado.html.twig', [
                'servidores' => $servidores,
                'controladoras' => $controladoras,
                'aps' => $aps
            ]);
        }

        $aps = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->findBy([
                'tipoEquipo' => TipoEquipo::AP
            ]);

        $apsUP = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->findBy([
                'tipoEquipo' => TipoEquipo::AP,
                'estado' => Estado::UP
            ]);

        $apsDOWN = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->findBy([
                'tipoEquipo' => TipoEquipo::AP,
                'estado' => Estado::DOWN
            ]);

        $dependencias = $this->getDoctrine()
            ->getRepository(Dependencia::class)
            ->findAll();

        $usuarios = $this->getDoctrine()
            ->getRepository(Usuario::class)
            ->findAll();

        $apscch = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->getTotalAppsPorDependencia('CCH');

        return $this->render('Default/index.html.twig', [
            'aps' => $aps,
            'apsUP' => $apsUP,
            'apsDOWN' => $apsDOWN,
            'dependencias' => $dependencias,
            'usuarios' => $usuarios,
            'nodos' => ArregloDependencia::NODOS,
            'apscch' => $apscch
        ]);
    }

    /**
     * @Route("/{nodo}/get_total_aps_por_dependencia",
     *     name="defaul.get_total_aps_por_dependencia")
     * @Method("GET")
     *
     */
    public function getTotalApsPorDependencia(string $nodo = null)
    {
        $dependencias = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->getTotalAppsPorDependencia($nodo);

        return new JsonResponse([
            'dependencias' => $dependencias
        ]);
    }
}
