<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 19/03/18
 * Time: 06:22 PM
 */
declare(strict_types=1);
namespace AppBundle\Controller;


use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioController extends Controller
{
    /**
     * @Route("/usuarios", name="usuario.listar")
     */
    public function listar()
    {
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class)
            ->findAll();

        return $this->render('Usuario/listar.html.twig', [
            'usuarios' => $usuarios
        ]);
    }

    /**
     * @Route("/usuarios/alta", name="usuario.alta")
     */
    public function alta(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(UsuarioType::class, null, [
            'action' => $this->generateUrl('usuario.alta')
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $usuario = $form->getData();
            $contrasena = $encoder->encodePassword($usuario, $usuario->getPlainPassword());
            $usuario->setContrasena($contrasena);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();
            $this->addFlash('success', "El usuario {$usuario->getUsuario()} se ha dado de alta correctamente");
            return $this->redirectToRoute('usuario.listar');
        }

        return $this->render('Usuario/alta.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/usuarios/{usuario}/edit", name="usuario.edit")
     * @ParamConverter("usuario", class="AppBundle\Entity\Usuario")
     */
    public function edit(Request $request, Usuario $usuario, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(UsuarioType::class, $usuario, [
            'action' => $this->generateUrl('usuario.edit', [
                'usuario' => $usuario->getId()
            ])
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($usuario->getPlainPassword() == null || empty(trim($usuario->getPlainPassword()))) {
                $contrasena = $usuario->getContrasena();
            } else {
                $contrasena = $encoder->encodePassword($usuario, $usuario->getPlainPassword());
            }
            $usuario->setContrasena($contrasena);

            $em->flush();
            $this->addFlash('success', "El usuario {$usuario->getUsuario()} se ha sido actualizado correctamente");
            return $this->redirectToRoute('usuario.listar');
        }

        return $this->render('Usuario/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/usuarios/{usuario}/eliminar", name="usuario.eliminar")
     * @ParamConverter("usuario", class="AppBundle\Entity\Usuario")
     */
    public function eliminar(Usuario $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($usuario);
        $em->flush();
        $this->addFlash('success', "El usuario {$usuario->getUsuario()} se ha eliminado");
        return $this->redirectToRoute('usuario.listar');
    }


    /**
     * @Route("/usuarios/get_csv", name="usuario.get_csv")
     */
    public function getCSV()
    {
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class)
            ->findAll();
        $view = $this->renderView('Usuario/csv', [
            'usuarios' => $usuarios
        ]);
        $response = new Response($view);
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;');
        $response->headers->set('Content-Disposition', 'attachment; filename=usuarios_csv.xlsx');
        return $response;
    }
}