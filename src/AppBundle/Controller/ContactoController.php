<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 1/04/18
 * Time: 08:21 PM
 */
declare(strict_types=1);
namespace AppBundle\Controller;


use AppBundle\Entity\Contacto;
use AppBundle\Form\ContactoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactoController extends Controller
{
    /**
     * @Route("/administradoras", name="contacto.listar")
     */
    public function listar()
    {
        $contactos = $this->getDoctrine()->getRepository(Contacto::class)
            ->findAll();
        return $this->render('Contacto/listar.html.twig', [
            'contactos' => $contactos
        ]);
    }

    /**
     * @Route("/administradoras/alta", name="contacto.alta")
     */
    public function alta(Request $request)
    {
        $form = $this->createForm(ContactoType::class, null, [
            'action' => $this->generateUrl('contacto.alta')
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $contacto = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush();
            $this->addFlash('success', "El contacto {$contacto->getNombre()} se ha dado de alta correctamente");
            return $this->redirectToRoute('contacto.listar');
        }

        return $this->render('Contacto/alta.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/administradoras/{contacto}/editar", name="contacto.editar")
     */
    public function editar(Request $request, Contacto $contacto)
    {
        $form = $this->createForm(ContactoType::class, $contacto, [
            'action' => $this->generateUrl('contacto.editar', [
                'contacto' => $contacto->getId()
            ])
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $contacto = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush();
            $this->addFlash('success', "El contacto {$contacto->getNombre()} se ha actualizado correctamente");
            return $this->redirectToRoute('contacto.listar');
        }

        return $this->render('Contacto/alta.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/administradoras/{contacto}/eliminar", name="contacto.eliminar")
     */
    public function eliminar(Contacto $contacto)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($contacto);
        $em->flush();
        $this->addFlash('success', "El contacto {$contacto->getNombre()} se ha eliminado correctamente");
        return $this->redirectToRoute('contacto.listar');
    }
}