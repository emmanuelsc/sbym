<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 25/03/18
 * Time: 09:36 PM
 */
declare(strict_types=1);
namespace AppBundle\Controller;

use AppBundle\ArregloDependencia;
use AppBundle\Entity\Dependencia;
use AppBundle\Form\DependenciaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DependenciaController extends Controller
{
    /**
     * @Route("/dependencias", name="dependencia.listar")
     */
    public function listar()
    {
        $dependencias = $this->getDoctrine()
            ->getRepository(Dependencia::class)
            ->findAll();

        return $this->render('Dependencia/listar.html.twig', [
            'dependencias' => $dependencias
        ]);
    }

    /**
     * @Route("/dependencias/alta", name="dependencia.alta")
     */
    public function alta(Request $request)
    {
        $form = $this->createForm(DependenciaType::class, null, [
            'action' => $this->generateUrl('dependencia.alta')
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dependencia = $form->getData();
            $dependencia->setFechaActualizacion(new \DateTime());
            $em->persist($dependencia);
            $em->flush();
            $this->addFlash('success', "La dependencia {$dependencia->getNombre()} se ha dado de alta correctamente");
            return $this->redirectToRoute('dependencia.listar');
        }

        return $this->render('Dependencia/alta.html.twig', [
            'form' => $form->createView(),
            'campus' => ArregloDependencia::CAMPUS,
            'nodos' => ArregloDependencia::NODOS
        ]);
    }


    /**
     * @Route("/dependencias/{dependencia}/edit", name="dependencia.edit")
     * @ParamConverter("dependencia", class="AppBundle\Entity\Dependencia")
     */
    public function edit(Request $request, Dependencia $dependencia)
    {
        $form = $this->createForm(DependenciaType::class, $dependencia, [
            'action' => $this->generateUrl('dependencia.edit', [
                'dependencia' => $dependencia->getId()
            ])
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $dependencia->setFechaActualizacion(new \DateTime());
            $em->persist($dependencia);
            $em->flush();
            $this->addFlash('success', "La dependencia {$dependencia->getNombre()} se ha actualizado correctamente");

            return $this->redirectToRoute('dependencia.listar');
        }

        return $this->render('Dependencia/edit.html.twig', [
            'form' => $form->createView(),
            'campus' => ArregloDependencia::CAMPUS,
            'nodos' => ArregloDependencia::NODOS
        ]);
    }

    /**
     * @Route("/dependencias/{dependencia}/eliminar", name="dependencia.eliminar")
     * @ParamConverter("dependencia", class="AppBundle\Entity\Dependencia")
     */
    public function eliminar(Dependencia $dependencia)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($dependencia);
        $em->flush();
        $this->addFlash('success', "La dependencia {$dependencia->getNombre()} se ha eliminado correctamente");
        return $this->redirectToRoute('dependencia.listar');
    }
}