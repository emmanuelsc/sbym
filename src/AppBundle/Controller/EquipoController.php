<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 25/03/18
 * Time: 10:34 PM
 */
declare(strict_types=1);
namespace AppBundle\Controller;

use AppBundle\Entity\Equipo;
use AppBundle\Entity\TipoEquipo;
use AppBundle\Entity\Usuario;
use AppBundle\Form\EquipoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/equipos")
 */
class EquipoController extends Controller
{
    /**
     * @Route("/equipos_escritorios", name="escritorio.listar")
     */
    public function listar()
    {
        $equipos = $this->getDoctrine()->getRepository(Equipo::class)
            ->findBy([
                'tipoEquipo' => TipoEquipo::ESCRITORIO
            ]);

        return $this->render('Equipo/listar.html.twig', [
            'equipos' => $equipos,
            'tipoEquipo' => TipoEquipo::ESCRITORIO
        ]);
    }

    /**
     * @Route("/controladoras", name="controladoras.listar")
     */
    public function listarControladoras()
    {
        $equipos = $this->getDoctrine()->getRepository(Equipo::class)
            ->findBy([
                'tipoEquipo' => TipoEquipo::CONTROLADOR
            ]);

        return $this->render('Equipo/listar.html.twig', [
            'equipos' => $equipos,
            'tipoEquipo' => TipoEquipo::CONTROLADOR
        ]);
    }

    /**
     * @Route("/aps", name="aps.listar")
     */
    public function listarAps()
    {
        $equipos = $this->getDoctrine()->getRepository(Equipo::class)
            ->findBy([
                'tipoEquipo' => TipoEquipo::AP
            ]);

        return $this->render('Equipo/listar_app.html.twig', [
            'equipos' => $equipos,
            'tipoEquipo' => TipoEquipo::AP
        ]);
    }

    /**
     * @Route("/equipo/{tipoEquipo}/alta", name="equipo.alta")
     */
    public function alta(Request $request, int $tipoEquipo)
    {
        $form = $this->createForm(EquipoType::class, null, [
            'action' => $this->generateUrl('equipo.alta', [
                'tipoEquipo' => $tipoEquipo
            ])
        ]);
        $form->handleRequest($request);
        if($form->isValid()) {
            $equipo = $form->getData();
            $equipo->setFechaActualizacion(new \DateTime());
            $equipo->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($equipo);
            $em->flush();
            $this->addFlash('success', "El equipo de escritorio {$equipo->getNombre()} se ha dado de alta correctamente");
            return $this->redirectToRouteTipoEquipo($tipoEquipo);
        }

        return $this->render('Equipo/alta.html.twig', [
            'form' => $form->createView(),
            'tipoEquipo' => $tipoEquipo
        ]);
    }

    /**
     * @Route("/equipo/{equipo}/edit", name="equipo.edit")
     * @ParamConverter("equipo", class="AppBundle\Entity\Equipo")
     */
    public function edit(Request $request, Equipo $equipo)
    {
        $form = $this->createForm(EquipoType::class, $equipo, [
            'action' => $this->generateUrl('equipo.edit', [
                'equipo' => $equipo->getId()
            ])
        ]);
        $form->handleRequest($request);
        if($form->isValid()) {
            $equipo = $form->getData();
            $equipo->setFechaActualizacion(new \DateTime());
            $equipo->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($equipo);
            $em->flush();
            $this->addFlash('success', "El equipo {$equipo->getNombre()} se ha dado de actualizado correctamente");
            return $this->redirectToRouteTipoEquipo($equipo->getTipoEquipo()->getId());
        }

        return $this->render('Equipo/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/equipo/{equipo}/eliminar", name="equipo.eliminar")
     */
    public function eliminar(Equipo $equipo)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($equipo);
        $em->flush();
        $this->addFlash('success', "La equipo {$equipo->getNombre()} se ha eliminado correctamente");

        return $this->redirectToRouteTipoEquipo($equipo->getTipoEquipo()->getId());
    }

    private function redirectToRouteTipoEquipo(int $idTipoEquipo)
    {
        switch($idTipoEquipo) {
            case TipoEquipo::AP:
                return $this->redirectToRoute('aps.listar');
            case TipoEquipo::CONTROLADOR:
                return $this->redirectToRoute('controladoras.listar');
            case TipoEquipo::ESCRITORIO:
                return $this->redirectToRoute('escritorio.listar');

        }
    }
}