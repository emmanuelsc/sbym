<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 14/04/18
 * Time: 06:26 PM
 */
declare(strict_types=1);
namespace AppBundle\Controller;

use AppBundle\Entity\Archivo;
use AppBundle\Entity\TipoArchivo;
use AppBundle\Form\ArchivoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/repositorio")
 */
class RepositorioController extends Controller
{
    /**
     * @Route("/", name="repositorio.listar")
     */
    public function listar()
    {
        $tipoArchivo = $this->getDoctrine()
            ->getRepository(TipoArchivo::class)
            ->findBy([], ['nombre' => 'asc']);

        return $this->render('Repositorio/listar.html.twig', [
            'tipoArchivo' => $tipoArchivo
        ]);
    }

    /**
     * @Route("/alta", name="repositorio.alta")
     */
    public function alta(Request $request)
    {
        $form = $this->createForm(ArchivoType::class, null, [
            'action' => $this->generateUrl('repositorio.alta')
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $archivo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $archivo->setUsuario($this->getUser());
            $em->persist($archivo);
            $em->flush();
            $this->addFlash('success', "El archivo {$archivo->getNombre()} se ha dado de alta correctamente");
            return $this->redirectToRoute('repositorio.listar');
        }

        return $this->render('Repositorio/alta.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{archivo}/edit", name="repositorio.edit")
     */
    public function edit(Request $request, Archivo $archivo)
    {
        $form = $this->createForm(ArchivoType::class, $archivo, [
            'action' => $this->generateUrl('repositorio.edit', [
                'archivo' => $archivo->getId()
            ])
        ]);

        $form->handleRequest($request);
        if($form->isValid()) {
            $archivo = $form->getData();
            $em = $this->getDoctrine()->getManager();
            if($archivo->getFile() !== null) {
                $archivo->setUrl('Nuevo archivo');
            }
            $archivo->setUsuario($this->getUser());
            $em->persist($archivo);
            $em->flush();
            $this->addFlash('success', "El archivo {$archivo->getNombre()} se ha sido actualizado correctamente");
            return $this->redirectToRoute('repositorio.listar');
        }

        return $this->render('Repositorio/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{archivo}/eliminar", name="repositorio.eliminar")
     */
    public function eliminar(Archivo $archivo)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($archivo);
        $em->flush();
        $this->addFlash('success', "El archivo {$archivo->getNombre()} se ha eliminado");
        return $this->redirectToRoute('repositorio.listar');
    }
}