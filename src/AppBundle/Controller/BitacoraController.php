<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 21/04/18
 * Time: 09:01 PM
 */

namespace AppBundle\Controller;
use AppBundle\Entity\EquipoBitacora;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bitacora")
 */
class BitacoraController extends Controller
{
    /**
     * @Route("/", name="bitacora.listar")
     */
    public function listar()
    {
        $bitacora = $this->getDoctrine()
            ->getRepository(EquipoBitacora::class)
            ->findAll();
        return $this->render('Bitacora/listar.html.twig', [
            'bitacora' => $bitacora
        ]);
    }
}