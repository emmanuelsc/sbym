<?php
declare(strict_types=1);
namespace AppBundle\Form;

use AppBundle\Entity\Equipo;
use AppBundle\Entity\Estado;
use AppBundle\Entity\Grupo;
use AppBundle\Entity\Marca;
use AppBundle\Entity\Modelo;
use AppBundle\Entity\TipoEquipo;
use AppBundle\Entity\Usuario;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EquipoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('ip')
            ->add('mascara')
            ->add('gateway')
            ->add('ubicacion')
            ->add('inventario')
            ->add('serie')
            ->add('mac')
            ->add('fechaActualizacion')
            ->add('observaciones')
            ->add('estado', EntityType::class, [
                'class' => Estado::class,
                'choice_label' => 'nombre'
            ])
            ->add('marca', EntityType::class, [
                'class' => Marca::class,
                'choice_label' => 'nombre'
            ])
            ->add('modelo', EntityType::class, [
                'class' => Modelo::class,
                'choice_label' => 'nombre'
            ])
            ->add('tipoEquipo', EntityType::class, [
                'class' => TipoEquipo::class,
                'choice_label' => 'nombre'
            ])
            ->add('grupo', EntityType::class, [
                'class' => Grupo::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->join('d.dependencia', 'dependencia')
                        ->orderBy('d.nombre', 'ASC');
                },
                'choice_label' => 'nombre'
            ])
            ->add('controladora', EntityType::class, [
                'class' => Equipo::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.tipoEquipo = :idTipoEquipo')
                        ->orderBy('u.nombre', 'ASC')
                        ->setParameter('idTipoEquipo', TipoEquipo::CONTROLADOR);
                },
                'choice_label' => 'nombre'
            ])
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Equipo::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'equipo';
    }
}
