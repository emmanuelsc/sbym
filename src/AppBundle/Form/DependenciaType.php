<?php

namespace AppBundle\Form;

use AppBundle\Entity\Dependencia;
use AppBundle\Entity\Grupo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DependenciaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('campus')
            ->add('nodo')
            ->add('vlans')
            ->add('red')
            ->add('dhcp')
            ->add('mdhcp')
            ->add('gwdhcp')
            ->add('observaciones')
            ->add('grupos', EntityType::class, array(
                'class' => Grupo::class,
                'choice_label' => 'nombre',
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false
            ))
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Dependencia::class
        ));
    }

    public function getBlockPrefix()
    {
        return 'dependencia';
    }
}
