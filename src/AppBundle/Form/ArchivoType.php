<?php

namespace AppBundle\Form;

use AppBundle\Entity\Archivo;
use AppBundle\Entity\TipoArchivo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArchivoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('file', FileType::class)
            ->add('tipoArchivo', EntityType::class, [
                'class' => TipoArchivo::class,
                'choice_label' => 'nombre',
                'placeholder' => 'Elige una opción',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Archivo::class
        ));
    }

    public function getBlockPrefix()
    {
        return 'archivo';
    }
}
