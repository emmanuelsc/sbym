<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 21/04/18
 * Time: 08:48 PM
 */

namespace AppBundle;


interface IBitacora
{
    public function getNombre();
    public function getTabla();
}