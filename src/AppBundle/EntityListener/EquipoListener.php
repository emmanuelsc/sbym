<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 21/04/18
 * Time: 05:59 PM
 */

namespace AppBundle\EntityListener;

use AppBundle\Entity\Dependencia;
use AppBundle\Entity\Equipo;
use AppBundle\Entity\EquipoBitacora;
use AppBundle\IBitacora;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class EquipoListener
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function onFlush(OnFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {

            if($entity instanceof Equipo) {
                $this->persistBitacora(
                    $entity,
                    $em,
                    'Registro nuevo'
                );
            }

            if($entity instanceof Dependencia) {
                $this->persistBitacora(
                    $entity,
                    $em,
                    'Registro nuevo'
                );
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {

            foreach ($uow->getEntityChangeSet($entity) as $keyField => $field) {

                if($keyField === 'estado' ||
                    $keyField === 'modelo' ||
                    $keyField === 'marca'
                ) {
                    $this->persistBitacora(
                        $entity,
                        $em,
                        $keyField,
                        $field[0]->getNombre(),
                        $field[1]->getNombre()
                    );
                }

                elseif($this->tieneField($keyField) && $field[0] !== $field[1]) {
                    $this->persistBitacora(
                        $entity,
                        $em,
                        $keyField,
                        $field[0],
                        $field[1]
                    );
                }
            }
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if($entity instanceof Equipo) {
                $this->persistBitacora(
                    $entity,
                    $em,
                    'Registro eliminado'
                );
            }
        }
    }

    private function persistBitacora(
        IBitacora $IBitacora,
        EntityManager $em,
        string $campo = null,
        string $old = 'Registro nuevo',
        string $new = null
    )
    {
        $uow = $em->getUnitOfWork();
        $bitacora = new EquipoBitacora();
        $bitacora->setCampo($campo);
        $bitacora->setOld($old);
        $bitacora->setNew($new);
        $bitacora->setEquipo($IBitacora->getNombre());
        $bitacora->setTabla($IBitacora->getTabla());
        $bitacora->setFecha(new \DateTime());
        $bitacora->setUsuario($this->tokenStorage->getToken()->getUser()->getUsuario());
        $em->persist($bitacora);
        $classMetadata = $em->getClassMetadata(EquipoBitacora::class);
        $uow->computeChangeSet($classMetadata, $bitacora);
    }

    private function tieneField($keyField)
    {
        return $keyField === 'nombre' ||
            $keyField === 'ip' ||
            $keyField === 'mascara' ||
            $keyField === 'gateway' ||
            $keyField === 'ubicacion' ||
            $keyField === 'inventario' ||
            $keyField === 'serie' ||
            $keyField === 'mac' ||
            $keyField === 'observaciones' ||
            $keyField === 'red' ||
            $keyField === 'vlans' ||
            $keyField === 'dhcp' ||
            $keyField === 'mdhcp' ||
            $keyField === 'gwdhcp'
        ;
    }
}