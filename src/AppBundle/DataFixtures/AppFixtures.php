<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 1/04/18
 * Time: 05:55 PM
 */
declare(strict_types=1);
namespace AppBundle\DataFixtures;

use AppBundle\Entity\Estado;
use AppBundle\Entity\Grupo;
use AppBundle\Entity\Marca;
use AppBundle\Entity\Modelo;
use AppBundle\Entity\TipoEquipo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $modelo60 = new Modelo();
        $modelo60->setNombre('60');

        $modelo70 = new Modelo();
        $modelo70->setNombre('70');

        $modelo7200 = new Modelo();
        $modelo7200->setNombre('7200');

        $marcaAruba = new Marca();
        $marcaAruba->setNombre('Aruba');

        $marcaDell = new Marca();
        $marcaDell->setNombre('Dell');

        $marcaSupermicro = new Marca();
        $marcaSupermicro->setNombre('Supermicro');

        $tipoEquipoApp = new TipoEquipo();
        $tipoEquipoApp->setNombre('App');

        $tipoEquipoEscritorio = new TipoEquipo();
        $tipoEquipoEscritorio->setNombre('Escritorio');

        $tipoEquipoControladora = new TipoEquipo();
        $tipoEquipoControladora->setNombre('Controladora');

        $grupo = new Grupo();
        $grupo->setNombre('APG-FCA');
        $manager->persist($grupo);

        $estado = new Estado();
        $estado->setNombre('UP');
        $manager->persist($estado);

        $estado = new Estado();
        $estado->setNombre('DOWN');
        $manager->persist($estado);


        $manager->persist($modelo60);
        $manager->persist($modelo70);
        $manager->persist($modelo7200);

        $manager->persist($marcaAruba);
        $manager->persist($marcaDell);
        $manager->persist($marcaSupermicro);

        $manager->persist($tipoEquipoApp);
        $manager->persist($tipoEquipoEscritorio);
        $manager->persist($tipoEquipoControladora);

        $manager->flush();
    }
}