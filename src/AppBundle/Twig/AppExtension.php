<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 27/03/18
 * Time: 09:05 PM
 */

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('getIdsChildren', array($this, 'getIdsChildren')),
        );
    }

    public function getIdsChildren(array $collection) : array
    {
        $data = [];
        foreach($collection as $value) {
            $data[] = $value->vars['value'];
        }
        return $data;
    }
}