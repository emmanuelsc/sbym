/* ------------------------------------------------------------------------------
*
*  # Form wizard
*
*  Specific JS code additions for wizard_form.html page
*
*  Version: 1.1
*  Latest update: Dec 25, 2015
*
* ---------------------------------------------------------------------------- */

setTimeout(function () {

    // Checkboxes/radios (Uniform)
    // ------------------------------
    // Default initialization
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });

    // Danger
    $(".control-danger").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-danger-600 text-danger-800'
    });

    // Success
    $(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    // Warning
    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });

    // Info
    $(".control-info").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-info-600 text-info-800'
    });

    // Custom color
    $(".control-custom").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-indigo-600 text-indigo-800'
    });


    // Override defaults
    $.fn.selectpicker.defaults = {
        iconBase: '',
        tickIcon: 'icon-checkmark3'
    }



    // Basic setup
    // ------------------------------

    // Basic select
    $('.bootstrap-select').selectpicker();



    // Bootstrap switch
    // ------------------------------

    $(".switch").bootstrapSwitch();


    // Initialize plugins
    // ------------------------------

    // Select2 selects
    $('.select').select2();


    // Simple select without search
    $('.select-simple').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes and radios
    $('.styled').uniform({
        radioClass: 'choice'
    });


    // Styled file input
    $('.file-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });

    // Default initialization
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    // Input labels
    var $inputLabelAbsolute = $('.label-indicator-absolute input');

    // Output labels
    var $outputLabelAbsolute = $('.label-indicator-absolute > span');

    $.passy.requirements.length.min = 4;
    // Strength meter
    var feedback = [
        {color: '#D55757', text: 'Debíl', textColor: '#fff'},
        {color: '#EB7F5E', text: 'Normal', textColor: '#fff'},
        {color: '#3BA4CE', text: 'Bueno', textColor: '#fff'},
        {color: '#40B381', text: 'Fuerte', textColor: '#fff'}
    ];

    //
    // Setup strength meter
    //

    // Label indicator
    $inputLabelAbsolute.passy(function(strength) {
        $outputLabelAbsolute.text(feedback[strength].text);
        $outputLabelAbsolute.css('background-color', feedback[strength].color).css('color', feedback[strength].textColor);
    });

    // Label
    $('.generate-label-absolute').click(function() {
        $inputLabelAbsolute.passy( 'generate', 10 );
    });
}, 100);