/* ------------------------------------------------------------------------------
*
*  # Dynamic tree views
*
*  Specific JS code additions for extra_trees.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Drag and drop support
    $(".tree-drag").fancytree({
        extensions: ["dnd"],
        source: {
            url: "../../template/assets/demo_data/fancytree/fancytree.json"
        },
        dnd: {
            autoExpandMS: 400,
            focusOnClick: true,
            preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
            preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
            dragStart: function(node, data) {
                return true;
            },
            dragEnter: function(node, data) {
                return true;
            },
            dragDrop: function(node, data) {

                // This function MUST be defined to enable dropping of items on the tree.
                data.otherNode.moveTo(node, data.hitMode);

                console.log(node);
                console.log(data);

            }
        },
        init: function(event, data) {
            $('.has-tooltip .fancytree-title').tooltip();
        }
    });



    
});
