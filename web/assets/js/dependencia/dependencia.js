'use strict';

$(document).ready(function () {
    var NODOS = [
        'CCH',
        'ARQ',
        'DGTIC',
        'IIMAS',
        'ZC',
        'ENP',
        'EXT',
        'FES',
        'FOR'
    ];

    var $campus = $('#dependencia_campus'),
        $nodo = $('#dependencia_nodo');

    $campus.change(function (e) {
        $nodo.html('');
        if($(e.target).val() !== 'CU') {
            $nodo.append('<option value="' + NODOS[0] + '">' + NODOS[0] + '</option>');
            $nodo.append('<option value="' + NODOS[5] + '">' + NODOS[5] + '</option>');
            $nodo.append('<option value="' + NODOS[6] + '">' + NODOS[6] + '</option>');
            $nodo.append('<option value="' + NODOS[7] + '">' + NODOS[7] + '</option>');
            $nodo.append('<option value="' + NODOS[8] + '">' + NODOS[8] + '</option>');
            return;
        }
        $nodo.append('<option value="' + NODOS[1] + '">' + NODOS[1] + '</option>');
        $nodo.append('<option value="' + NODOS[2] + '">' + NODOS[2] + '</option>');
        $nodo.append('<option value="' + NODOS[3] + '">' + NODOS[3] + '</option>');
        $nodo.append('<option value="' + NODOS[4] + '">' + NODOS[4] + '</option>');

    });

});